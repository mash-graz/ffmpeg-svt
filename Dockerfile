# the instruction in this file are 
# based on intels OpenVisualCloud
# docker creation prozess for ffmpeg
# https://github.com/OpenVisualCloud/Dockerfiles/blob/master/Xeon/ubuntu-18.04/media/ffmpeg/Dockerfile
# and https://github.com/OpenVisualCloud/SVT-VP9/issues/97

from debian:testing AS build
WORKDIR /home


RUN sed -i 's/main/main contrib non-free/' /etc/apt/sources.list \
  && DEBIAN_FRONTEND=noninteractive apt update \
  && apt install -y -q --no-install-recommends \
  build-essential autoconf make git wget curl pciutils \
  cpio libtool lsb-release ca-certificates pkg-config \
  bison flex libcurl4-gnutls-dev zlib1g-dev cmake \
  automake nasm yasm libogg-dev libvorbis-dev \
  libmp3lame-dev libfdk-aac-dev libopus-dev libvpx-dev \
  libx264-dev libx265-dev libnuma-dev python3-pip ninja-build \
  meson libass-dev libfreetype6-dev libssl-dev libva-dev 

# Build dav1d
RUN git clone https://code.videolan.org/videolan/dav1d.git; \
  cd dav1d; \
  meson build --prefix /usr --libdir /usr/lib/x86_64-linux-gnu --buildtype release; \
  ninja -C build; \
  cd build; \
  DESTDIR="/home/build" ninja install; \
  ninja install;

# Fetch SVT-HEVC
ARG SVT_HEVC_VER=v1.4.3
ARG SVT_HEVC_REPO=https://github.com/intel/SVT-HEVC

RUN git clone ${SVT_HEVC_REPO} && \
    cd SVT-HEVC/Build/linux && \
    export PKG_CONFIG_PATH="/usr/local/lib/x86_64-linux-gnu/pkgconfig" && \
    git checkout ${SVT_HEVC_VER} && \
    mkdir -p ../../Bin/Release && \
    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local -DCMAKE_INSTALL_LIBDIR=lib/x86_64-linux-gnu -DCMAKE_ASM_NASM_COMPILER=yasm ../.. && \
    make -j8 && \
    make install DESTDIR=/home/build && \
    make install

# Fetch SVT-AV1
ARG SVT_AV1_VER=v0.8.3
ARG SVT_AV1_REPO=https://github.com/OpenVisualCloud/SVT-AV1

ARG SVT_AV1_PATCHES_RELEASE_VER=0.4
ARG SVT_AV1_PATCHES_RELEASE_URL=https://github.com/VCDP/CDN/archive/v${SVT_AV1_PATCHES_RELEASE_VER}.tar.gz
ARG SVT_AV1_PATCHES_PATH=/home/CDN-${SVT_AV1_PATCHES_RELEASE_VER}
RUN wget -O - ${SVT_AV1_PATCHES_RELEASE_URL} | tar xz

RUN git clone ${SVT_AV1_REPO} && \
    cd SVT-AV1 && \
    git checkout ${SVT_AV1_VER} && \
    find ${SVT_AV1_PATCHES_PATH}/SVT-AV1_patches -type f -name '*.patch' -print0 | sort -z | xargs -t -0 -n 1 patch -p1 -i && \
    cd Build/linux && \
    mkdir -p ../../Bin/Release && \
    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local -DCMAKE_INSTALL_LIBDIR=lib/x86_64-linux-gnu -DCMAKE_ASM_NASM_COMPILER=yasm ../.. && \
    make -j8 && \
    make install DESTDIR=/home/build && \
    make install 

# Fetch SVT-VP9
ARG SVT_VP9_REPO=https://github.com/OpenVisualCloud/SVT-VP9

RUN git clone ${SVT_VP9_REPO} && \
    cd SVT-VP9/Build/linux && \
    mkdir -p ../../Bin/Release && \
    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local -DCMAKE_INSTALL_LIBDIR=lib/x86_64-linux-gnu -DCMAKE_ASM_NASM_COMPILER=yasm ../.. && \
    make -j8 && \
    make install DESTDIR=/home/build && \
    make install 

#Remove build residue from SVT-AV1 build -- temp fix for bug
RUN if [ -d "build/home/" ]; then rm -rf build/home/; fi

RUN git config --global user.name 'anonymous robot' \
    && git config --global user.email '<>' \
    && git clone https://github.com/FFmpeg/FFmpeg.git \
    && cd /home/FFmpeg \
    && curl -L \
    "https://gist.githubusercontent.com/1480c1/ecc1dd3a15093398492c86f2314f4a67/raw/0001-Add-ability-for-ffmpeg-to-run-svt-av1.patch" \
    "https://gist.githubusercontent.com/1480c1/ecc1dd3a15093398492c86f2314f4a67/raw/0001-Add-ability-for-ffmpeg-to-run-svt-vp9.patch" \
    "https://gist.githubusercontent.com/1480c1/ecc1dd3a15093398492c86f2314f4a67/raw/0001-lavc-svt_hevc-add-libsvt-hevc-encoder-wrapper.patch" \
    | git am

# Compile FFmpeg
RUN cd /home/FFmpeg \
    && export PKG_CONFIG_PATH="/usr/local/lib/x86_64-linux-gnu/pkgconfig" && \
    ./configure --prefix="/usr/local" --extra-cflags="-w" --extra-ldflags="" \
    --libdir=/usr/local/lib/x86_64-linux-gnu --extra-libs="-lpthread -lm" \
    --enable-shared --enable-gpl --enable-libass --enable-libfreetype \
    --disable-xlib --disable-sdl2 --enable-openssl --enable-nonfree --enable-vaapi \
    --enable-hwaccels --disable-doc --disable-htmlpages --disable-manpages \
    --disable-podpages --disable-txtpages --enable-libfdk-aac --enable-libmp3lame \
    --enable-libopus --enable-libvorbis --enable-libvpx --enable-libx264 --enable-libx265 \
    --enable-libdav1d --enable-libsvthevc --enable-libsvtav1  --enable-libsvtvp9 && \
    make -j8 && \
    make install && make install DESTDIR="/home/build"

FROM debian:testing
WORKDIR /home

RUN ln -sf /usr/share/zoneinfo/UTC /etc/localtime \
    && sed -i 's/main/main contrib non-free/' /etc/apt/sources.list \
    && DEBIAN_FRONTEND=noninteractive apt update \
    && apt install -y -q --no-install-recommends \
    libnuma1 libass9 libssl1.1 libpciaccess0 \
    libva2 libva-drm2 libx264-155 libx265-179 libvpx6 libfdk-aac2 \
    libmp3lame0 libopus0 libvorbis0a libvorbisenc2 libvorbisfile3 \
    && rm -rf /var/lib/apt/lists/*

# Install
COPY --from=build /home/build /
ENV LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib:/usr/local/lib/x86_64-linux-gnu
